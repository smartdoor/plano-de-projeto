package com.oVoo.oovoosample.bluetooth;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ooVoo.oovoosample.R;


public class BluetoothMainActivity extends Activity 
{
	Thread workerThread;
  	byte[] readBuffer;
  	int readBufferPosition;
  	int counter;
    int bytes;
    boolean detectarPresenca = false;
  	volatile boolean stopWorker;
	private final int REQUEST_CONNECT_DEVICE = 1;
	private final int REQUEST_ENABLE_BT = 2;
	private final int REQUEST_VIDEO_CHAMADA = 3;
	private final int REQUEST_ENABLE_BT2 = 4;
	private BluetoothAdapter meuAdaptadorBluetooth = null;

    private BluetoothSocket mmSocket = null;
    private BluetoothDevice mmDevice = null;

    private InputStream mmInStream = null;
    private DataOutputStream mmOutStream = null;
    TextView stateBluetooth;
    private Button conectar = null;
    private Button pareado = null;
    private  Button receive = null;
    private static String address = "Oi";
    private static boolean pronto = false; //tem endereco do bluetooth
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private static int ajuda = 0; 

    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
		
        if(ajuda > 0)
        {
      		Intent serverIntent2 = new Intent();
    		serverIntent2.setClass(BluetoothMainActivity.this, com.oVoo.oovoosample.bluetooth.BluetoothMain2.class);
    		BluetoothMainActivity.this.startActivityForResult(serverIntent2, REQUEST_VIDEO_CHAMADA);
        }

        meuAdaptadorBluetooth = BluetoothAdapter.getDefaultAdapter();
        
        conectar = (Button) findViewById(R.id.conectar);
        pareado = (Button) findViewById(R.id.button3);
        receive = (Button) findViewById(R.id.receber);
        
        stateBluetooth = (TextView)findViewById(R.id.bluetoothstate);
        //CheckBluetoothState();
        
        conectar.setOnClickListener(Conexao);
        pareado.setOnClickListener(Pareado);
        receive.setOnClickListener(receberDados);
        
        
    }
    
	@Override
	public void onBackPressed() 
	{
	}

    
    private Button.OnClickListener Conexao = new Button.OnClickListener()
    {
	  @Override
		  public void onClick(View arg0) 
		  {
		  	  if(meuAdaptadorBluetooth.isEnabled())
		  	  {
				  Intent serverIntent = new Intent();
			      serverIntent.setClass(BluetoothMainActivity.this, DeviceListActivity.class);
			      BluetoothMainActivity.this.startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
		  	  }
			  else
			  {
				  Intent enableBtIntent = new Intent(meuAdaptadorBluetooth.ACTION_REQUEST_ENABLE);
	  			  startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT2);
			  }

		  }
	};

    private Button.OnClickListener Pareado = new Button.OnClickListener()
    {
	  @Override
		  public void onClick(View arg0) 
		  {
		  // TODO Auto-generated method stub
			  if (meuAdaptadorBluetooth.isEnabled())
			  {
				  Intent serverIntent = new Intent();
			      serverIntent.setClass(BluetoothMainActivity.this, Pareados.class);
			      BluetoothMainActivity.this.startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			  }
			  else
			  {
				  Intent enableBtIntent = new Intent(meuAdaptadorBluetooth.ACTION_REQUEST_ENABLE);
	  			  startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			  }

		  }
	};

    public void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        switch (requestCode) 
        {
        case REQUEST_CONNECT_DEVICE:
            if (resultCode == Activity.RESULT_OK) 
            {
            	
            	meuAdaptadorBluetooth.cancelDiscovery();
                address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                mmDevice = meuAdaptadorBluetooth.getRemoteDevice(address);
                
                try 
                {
                	mmSocket = mmDevice.createRfcommSocketToServiceRecord(MY_UUID);
                	mmSocket.connect();
                	
                	mmInStream = mmSocket.getInputStream();
                    mmOutStream = new DataOutputStream(mmSocket.getOutputStream());
                    pronto = true;
                }
                catch(IOException e)
                {
                	Toast.makeText(this, "Ocorreu um erro!", Toast.LENGTH_LONG).show();
                }
                
            }
            break;
        case REQUEST_ENABLE_BT:
        {
        	if(meuAdaptadorBluetooth.isEnabled())
        	{
	  			  Intent serverIntent = new Intent();
			      serverIntent.setClass(BluetoothMainActivity.this, Pareados.class);
			      BluetoothMainActivity.this.startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			      
        	}
        	break;
        }
        case REQUEST_ENABLE_BT2:
        {
		  	  if(meuAdaptadorBluetooth.isEnabled())
		  	  {
				  Intent serverIntent = new Intent();
			      serverIntent.setClass(BluetoothMainActivity.this, DeviceListActivity.class);
			      BluetoothMainActivity.this.startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
		  	  }
		  	break;
        }
        }
    }
    

    private Button.OnClickListener receberDados = new Button.OnClickListener()
    {
    	  @Override
		  public void onClick(View arg0) 
		  {
		  		if(!pronto)
		  		{
		  			Context context = getApplicationContext();
		  			Toast.makeText(context, "Conecte-se Primeiro", Toast.LENGTH_LONG).show();
		  				
		  		}
		  		else
		  		{
		  			setContentView(R.layout.activity_main_logo);
		  			beginListenForData();
			  		
		  		}
		  }
	};
	
	private void beginListenForData() 
	{

	    workerThread = new Thread(new Runnable() 
	    {
	      public void run() 
	      {
	         while(!detectarPresenca) 
	         {
	        	 try 
	        	 {
	        		int numeroBytes = mmInStream.available();
	                if(numeroBytes > 0)
	                {
	                    detectarPresenca = true;
	                }
	              	            
	        	 } 
	        	 catch (IOException ex) 
	        	 {
	        	 }
	         }
	         detectarPresenca = false;
	         ajuda++;
	         try
	         {
				mmOutStream.close();
				mmInStream.close();
		        mmSocket.close();
			} catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         
	  		Intent serverIntent = new Intent();
			serverIntent.setClass(BluetoothMainActivity.this, com.ooVoo.oovoosample.Main.MainActivity.class);
			BluetoothMainActivity.this.startActivityForResult(serverIntent, REQUEST_VIDEO_CHAMADA);
	
	      }
	    });

	    workerThread.start();
	  }
}