package com.oVoo.oovoosample.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;  
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import com.ooVoo.oovoosample.R;

public class BluetoothMain2 extends Activity {
  TextView myLabel;
  EditText myTextbox;
  BluetoothAdapter mBluetoothAdapter;
  BluetoothSocket mmSocket;
  BluetoothDevice mmDevice;
  OutputStream mmOutputStream;
  InputStream mmInputStream;
  Thread workerThread;
  private static boolean detectarPresenca = false; 

  @Override
  public void onCreate(Bundle savedInstanceState) 
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main_logo);
    detectarPresenca = false;

    findBT();
	openBT();
	beginListenForData();
  }
  
	@Override
	public void onBackPressed() {
	}
  void findBT() 
  {
    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
    if(pairedDevices.size() > 0) 
    {
      for(BluetoothDevice device : pairedDevices) 
      {
        if(device.getName().equals("HC-06")) 
        {
          mmDevice = device;
          break;
        }
      }
    }
  }

  void openBT()
  {
    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //Standard //SerialPortService ID
    mBluetoothAdapter.cancelDiscovery();
   
    try
    {
        mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);    
        mmSocket.connect();
        mmOutputStream = mmSocket.getOutputStream();
        mmInputStream = mmSocket.getInputStream();
    }
    catch(IOException e)
    {
    	Toast.makeText(this, "Ocorreu um erro!", Toast.LENGTH_LONG).show();
    }
  }

  void beginListenForData() 
  {

    workerThread = new Thread(new Runnable() 
    {
      public void run() 
      {
         while(!detectarPresenca) 
         {
	          try 
	          {
	            int bytesAvailable = mmInputStream.available();            
	            if(bytesAvailable > 0) 
	            {
	

		  			detectarPresenca = true;

	            }
	          } 
	          catch (IOException ex) 
	          {
	          }
         }
         try
         {
			mmOutputStream.close();
			mmInputStream.close();
	        mmSocket.close();
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  		Intent serverIntent = new Intent();
			serverIntent.setClass(BluetoothMain2.this, com.ooVoo.oovoosample.Main.MainActivity.class);
			BluetoothMain2.this.startActivityForResult(serverIntent,0);
      }
      
    });

    workerThread.start();
    
  }

  void closeBT() throws IOException 
  {

    mmOutputStream.close();
    mmInputStream.close();
    mmSocket.close();
  }

}