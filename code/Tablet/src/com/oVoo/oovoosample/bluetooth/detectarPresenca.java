package com.oVoo.oovoosample.bluetooth;


import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import com.oVoo.oovoosample.bluetooth.*;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;



public class detectarPresenca extends AsyncTask<Void,Void,Void>
{
	
	private String address;
	
    private BluetoothSocket mmSocket = null;
    private BluetoothDevice mmDevice = null;
    private InputStream mmInStream = null;
    private BluetoothAdapter meuAdaptadorBluetooth = null;

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
	
    detectarPresenca(String aux)
	{
		this.address = aux;
	}
	
	protected Void doInBackground(Void... arg) 
	{
        int bytes;
        boolean detectarPresenca = false;
        
        meuAdaptadorBluetooth = BluetoothAdapter.getDefaultAdapter();
        meuAdaptadorBluetooth.cancelDiscovery();
     
        mmDevice = meuAdaptadorBluetooth.getRemoteDevice(address);
                
        try 
        {
        	mmSocket = mmDevice.createRfcommSocketToServiceRecord(MY_UUID);
        	mmSocket.connect();
        	mmInStream = mmSocket.getInputStream();				
        }
        catch(IOException e){}
        
        while (!detectarPresenca) 
        {
            try 
            {
            	int numeroBytes = mmInStream.available();
                if(numeroBytes > 0)
                {
                	byte[] read = new byte[numeroBytes];
                	bytes = mmInStream.read(read);
                    String readMessage = new String(read);
                    detectarPresenca = true;
                }
            } 
            catch (IOException e) 
            {
            }
        }  
		return null;
		
	}

}
