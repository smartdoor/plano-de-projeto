//
// InformationActivity.java
// 
// Created by ooVoo on July 22, 2013
//
// © 2013 ooVoo, LLC.  Used under license. 
//
package com.ooVoo.oovoosample.Information;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ooVoo.oovoosample.Common.Participant;
import com.ooVoo.oovoosample.Common.Utils;
import com.ooVoo.oovoosample.VideoCall.VideoCallActivity;
import com.ooVoo.oovoosample.ConferenceManager;
import com.ooVoo.oovoosample.ConferenceManager.SessionParticipantsListener;
import com.ooVoo.oovoosample.R;
import com.oovoo.core.ConferenceCore.FrameSize;
import com.oovoo.core.IConferenceCore.ConferenceCoreError;

// Information presenter entity
public class InformationActivity extends Activity implements SessionParticipantsListener
{

	private CustomParticipantAdapter mCustomAdapter;
	private TextView mSessionIdView = null;	 
	private ConferenceManager mConferenceManager = null;
	private Button aceitar;
	private boolean pronto = false;
	private int cont = 0;
	private Notification.Builder mBuilder ;
	private boolean onpause = false;
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{		
		super.onCreate(savedInstanceState);
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		//pronto = false;
		onpause = false;
		//cont = 0;
		
		mBuilder =
			    new Notification.Builder(this)
			    .setSmallIcon(R.drawable.ic_main)
			    .setContentTitle("Chamada de Visitante")
			    .setContentText("Clique aqui para aceitar");
		
		Intent resultIntent = new Intent(this, VideoCallActivity.class);
		
		PendingIntent resultPendingIntent =
			    PendingIntent.getActivity(
			    this,
			    0,
			    resultIntent,
			    PendingIntent.FLAG_UPDATE_CURRENT
			);
		
		mBuilder.setContentIntent(resultPendingIntent);
		
		initView();
		

	}
	
	
	private void initView(){
		// Set layout
		setContentView(R.layout.information);
		mConferenceManager = ConferenceManager.getInstance(getApplicationContext());	
		mSessionIdView = (TextView) findViewById(R.id.sessionIdInformationValueLabel);		
		mCustomAdapter = new CustomParticipantAdapter(mConferenceManager.getActiveUsers());
		aceitar = (Button) findViewById(R.id.button1);
		aceitar.setOnClickListener(rodar);

		ActionBar ab = getActionBar();
		if(ab != null)
		{
			ab.setTitle(R.string.information_screen_name);
			ab.setDisplayShowTitleEnabled(true);
			ab.setDisplayUseLogoEnabled(false);
			ab.setIcon(R.drawable.ic_main);
		}
	}
	
	 private Button.OnClickListener rodar = new Button.OnClickListener()
	 {
		  @Override
		  public void onClick(View arg0) 
		  {   
		  	if(pronto)
		  	{
		  		pronto = false;
		  		Intent serverIntent = new Intent();
			    serverIntent.setClass(InformationActivity.this, com.ooVoo.oovoosample.VideoCall.VideoCallActivity.class);
			    startActivity(serverIntent);
			    
			    finish();
		  	}
		  	else
		  	{
		  		Context context = getApplicationContext();
	  			Toast.makeText(context, "Ningu�m na chamada", Toast.LENGTH_LONG).show();
		  	}

		 }
	};
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item == null)
			return false;

		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		onpause = false;
		mConferenceManager.addSessionParticipantsListener(this);
		mSessionIdView.setText(mConferenceManager.retrieveSettings().SessionID);
		mCustomAdapter.updateParticipants(mConferenceManager.getActiveUsers());	
		

	}
	public void onBackPressed() 
	{
		mConferenceManager.endOfCall();
		finish();
		super.onBackPressed();
		
	}
	
	@Override
	protected void onPause() 
	{
		super.onPause();
		onpause = true;
		mConferenceManager.addSessionParticipantsListener(this);
		mCustomAdapter.updateParticipants(mConferenceManager.getActiveUsers());	
	}

	private void refershList() {	
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(mCustomAdapter != null){
					mCustomAdapter.updateParticipants(mConferenceManager.getActiveUsers());
					mCustomAdapter.notifyDataSetChanged();

				}
			}
		});		
	}
	
	private void updateDataInList() {	
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if(mCustomAdapter != null){
					mCustomAdapter.updateParticipants(mConferenceManager.getActiveUsers());
					mCustomAdapter.notifyDataSetChanged();
				}
			}
		});		
	}
	

	
	@Override
	public void onParticipantVideoTurnedOn(ConferenceCoreError eErrorCode, String sParticipantId, FrameSize frameSize, int participantViewId, String displayName) {
		refershList();
	}
	
	@Override
	public void onParticipantVideoTurnedOff(ConferenceCoreError eErrorCode, int participantViewId, String sParticipantId) {
		refershList();
	}
	
	@Override
	public void onParticipantJoinedSession(String sParticipantId, int participantViewId, String sOpaqueString) 
	{
		pronto = true;
		if(cont == 0)
		{
	        Vibrator v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
	        v.vibrate(2000);
	        cont = 1;
		}
		if(onpause)
		{
			int mNotificationId = 001;
			NotificationManager mNotifyMgr = 
			        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			mNotifyMgr.notify(mNotificationId, mBuilder.build());
		}


		updateDataInList();
	}
	
	@Override
	public void onParticipantLeftSession(int participantViewId, String sParticipantId){
		pronto = false;
		cont = 0;
		updateDataInList();
	}

	@Override
	public void onParticipantVideoPaused(int participantViewId) {		
	}

	@Override
	public void onParticipantVideoResumed(int participantViewId, String sParticipantId) {		
	}
	
	private void switchParticipantVideoOn(Participant participant){
		Log.d(Utils.getOoVooTag(), "turning video on for " + participant.getDisplayName());
		mConferenceManager.turnParticipantVideoOn(participant.getId());		
	}

	private void switchParticipantVideoOff(Participant participant){
		Log.d(Utils.getOoVooTag(), "turning video off for " + participant.getId());
		mConferenceManager.turnParticipantVideoOff(participant.getId(), participant.getDisplayName());		
	}
	
	public class CustomParticipantAdapter extends BaseAdapter implements OnCheckedChangeListener{

		Participant[] listArray;

		public CustomParticipantAdapter(Participant[] participants) {
			listArray = participants;
		}
		
		public void updateParticipants(Participant[] participants){
			listArray = participants;
		}

		@Override
		public int getCount() {
			return (listArray == null)? 0 : listArray.length; // total number of elements in the list
		}

		@Override
		public Object getItem(int i) {
			return (listArray == null)? null : listArray[i]; // single item in the list
		}

		@Override
		public long getItemId(int i) {
			return i; // index number
		}

		@Override
		public View getView(int index, View view, final ViewGroup parent) {

			ParticipantViewHolder viewHolder = null;
			if (view == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				view = inflater.inflate(R.layout.participantinfo, parent, false);
				viewHolder = new ParticipantViewHolder();
				viewHolder.textView = (TextView) view.findViewById(R.id.participantId);
				view.setTag(viewHolder);			
			}
			else{
				viewHolder = (ParticipantViewHolder)view.getTag();
			}

			final Participant participant = listArray[index];	
  			if(participant.getDisplayName() != "Me")
			{
  				pronto = true;
  				viewHolder.textView.setText("");
			}
					
			
			return view;
		}
		


		
		private class ParticipantViewHolder
		{
			//Switch button;
			TextView textView;
		}
		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) 
		{
		}
		
	   
	}
	
}
